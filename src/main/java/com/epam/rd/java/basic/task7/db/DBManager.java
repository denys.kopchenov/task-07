package com.epam.rd.java.basic.task7.db;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static Connection getConnection() throws SQLException {
        Connection connection = null;
        try (FileReader fileReader = new FileReader("app.properties")) {
            Properties properties = new Properties();
            properties.load(fileReader);
            connection = DriverManager.getConnection(properties.getProperty("connection.url"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return connection;
    }


    public List<User> findAllUsers() throws DBException {
        List<User> allUsers = new ArrayList<>();
        String selectAllUsers = "SELECT id, login FROM users ORDER BY id";
        try (Connection connection = DBManager.getConnection();
             ResultSet resultSet = connection.prepareStatement(selectAllUsers).executeQuery()) {
            while (resultSet.next()) {
                String login = resultSet.getString("login");
                User user = new User(login);
                user.setId(resultSet.getInt("id"));
                allUsers.add(user);
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return allUsers;
    }

    public boolean insertUser(User user) throws DBException {
        String insertUser = "INSERT INTO users (login) VALUES (?)";
        try (Connection connection = DBManager.getConnection();
             PreparedStatement prepareStatement = connection.prepareStatement(insertUser, Statement.RETURN_GENERATED_KEYS)) {
            prepareStatement.setString(1, user.getLogin());
            prepareStatement.executeUpdate();
            ResultSet rs = prepareStatement.getGeneratedKeys();
            if (rs.next()) {
                user.setId(rs.getInt(1));
            }
            rs.close();
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return true;
    }


    public boolean deleteUsers(User... users) throws DBException {
        String DELETE_USERS = "DELETE FROM users WHERE login = ?";
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USERS)){
            for (User user :
                    users) {
                preparedStatement.setString(1, user.getLogin());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = new User(login);
        String SELECT_USER = "SELECT id FROM users WHERE login = ?";
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
            }
            resultSet.close();
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team(name);
        String SELECT_TEAM = "SELECT id FROM teams WHERE name = ?";
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TEAM)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                team.setId(resultSet.getInt("id"));
            }
            resultSet.close();
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> allTeams = new ArrayList<>();
        String selectAllTeams = "SELECT id, name FROM teams ORDER BY id";
        try (Connection connection = DBManager.getConnection();
             ResultSet resultSet = connection.prepareStatement(selectAllTeams).executeQuery()) {
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                Team team = new Team(name);
                team.setId(resultSet.getInt("id"));
                allTeams.add(team);
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return allTeams;
    }

    public boolean insertTeam(Team team) throws DBException {
        String insertUser = "INSERT INTO teams (name) VALUES (?)";
        try (Connection connection = DBManager.getConnection();
             PreparedStatement prepareStatement = connection.prepareStatement(insertUser, Statement.RETURN_GENERATED_KEYS)) {
            prepareStatement.setString(1, team.getName());
            prepareStatement.executeUpdate();
            ResultSet rs = prepareStatement.getGeneratedKeys();
            if (rs.next()) {
                team.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection connection = null;
        String SET_TEAMS_FOR_USER = "INSERT INTO users_teams VALUES (?, ?)";
        try {
            connection = DBManager.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SET_TEAMS_FOR_USER);
            connection.setAutoCommit(false);
            int userID = user.getId();
            for (Team team :
                    teams) {
                preparedStatement.setInt(1, userID);
                preparedStatement.setInt(2, team.getId());
                preparedStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    throw new DBException(ex.getMessage(), ex);
                }
            }
            throw new DBException(e.getMessage(), e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> userTeams = new ArrayList<>();
        String SELECT_USER_TEAMS = "SELECT teams.name, teams.id FROM teams INNER JOIN users_teams ON users_teams.team_id = teams.id WHERE user_id = ?";
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_TEAMS)) {
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Team team = new Team(resultSet.getString("name"));
                team.setId(resultSet.getInt("id"));
                userTeams.add(team);
            }
            resultSet.close();
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return userTeams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        String DELETE_TEAM = "DELETE FROM teams WHERE name = ?";
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TEAM)){
            preparedStatement.setString(1, team.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";
        try (Connection connection = DBManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TEAM)){
            preparedStatement.setString(1, team.getName());
            preparedStatement.setInt(2, team.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
        return true;
    }

}
